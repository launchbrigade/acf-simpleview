<?php
/**
 * ACF SimpleView plugin file.
 *
 * @package ACFSV\Main
 */
ini_set('max_execution_time', 900);

if (! function_exists('add_filter')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

if (!defined('ACFSV_PATH')) {
    define('ACFSV_PATH', dirname(__FILE__));
}

if (!defined('ACFSV_CONFIG')) {
    define('ACFSV_CONFIG', ACFSV_PATH . '/simpleview.json');
}

if (!defined('ACFSV_LISTINGS')) {
    define('ACFSV_LISTINGS', ACFSV_PATH . '/acf-listing.json');
}

require_once ACFSV_PATH . "/src/simpleview.php";

if (is_admin()) {
    include_once(ACFSV_PATH . '/admin/admin.php');
}

if (!function_exists('Acfsv_Load_listings_structure')) {
    /**
     * Load the ACF Listing Structure so we can create listing posts
     *
     * @return object $jsonStr
     */
    function Acfsv_Load_listings_structure()
    {
        print ACFSV_LISTINGS . "\n";
        $jsonStr = file_get_contents(ACFSV_LISTINGS);
        return json_decode($jsonStr);
    }
}

// $config = Acfsv_Load_listings_structure();
// print_r($config);