<?php
/**
 * Advanced Custom Fields for SimpleView
 *
 * @package ACFSV
 * @author Darren Odden <darren@odden.io>
 * @license MIT
 *
 * @wordpress-plugin
 * Plugin Name: Advanced Custom Fields - SimpleView
 * Plugin URI: https://www.launchbrigade.com
 * Description: Add SimpleView data mapping data
 * Version: 1.0.0
 * Author: Darren Odden
 * Author URI: https://www.odden.io
 * Text Domain: acfsv
 */

if (!function_exists('add_filter')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

if (!defined('ACFSV_FILE')) {
    define('ACFSV_FILE', __FILE__);
}

require_once __DIR__ . '/acfsv-main.php';


function Acfsv_activation()
{
    if (!wp_next_scheduled('acfsv_daily_listings_import')) {
        wp_schedule_event(time(), 'daily', 'acfsv_daily_listings_import');
    }
}

if (!function_exists('Acfsv_daily_listings_import')) {
    function Acfsv_daily_listings_import()
    {
        ACFSV_getAllListings();
    }
    add_action('acfsv_daily_listings_import', 'Acfsv_daily_listings_import');
}

function Acfsv_deactivation()
{
    wp_clear_scheduled_hook('acfsv_daily_listings_import');
}