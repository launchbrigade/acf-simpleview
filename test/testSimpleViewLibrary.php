<?php

use SimpleView\SimpleViewLibrary;

/**
 * SimpleView Library Structure test
 */

// require_once "../vendor/simpleview-php/src/simpleViewLibrary.php";
require_once "../acfsv-main.php";

/**
 * Undocumented function
 *
 * @return object
 */
function getConfig($config)
{
    $jsonStr = file_get_contents($config);
    return json_decode($jsonStr);
}



$config = getConfig(__DIR__ . "/../simpleview.json");
$svl = new SimpleViewLibrary($config);
$types = $svl->getListingTypes(1);
// print_r($types);
// foreach ($types["DATA"] as $type) {
//     print $type["TYPEID"] . " : " . $type["NAME"] . "\n";
// }

$cat = $svl->getListingCategories(0);
// print_r($cat["DATA"]);
foreach ($cat["DATA"] as $type) {
    print $type["LISTINGTYPEID"] . " : " . $type["CATID"] . " : " . $type["NAME"] . "\n";
}

// $subCat = $svl->getListingSubCategories(0, 0);
// print_r($subCat);
// foreach ($subCat["DATA"] as $type) {
//     print $type["LISTINGTYPEID"] . " : " . $type["CATID"] . " : " . $type["SUBCATID"] . " : " . $type["SUBCATNAME"] . "\n";
// }

print "\n";
