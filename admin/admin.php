<?php
// hello
require_once ACFSV_PATH . '/src/simpleview.php';

if (! class_exists('ACFSV_admin')) {
    class ACFSV_admin
    {
        public function __construct()
        {
            add_action('admin_menu', array($this, 'admin_menu'));
            
            add_action( 'admin_post_listing_import', 'acfsv_admin_listing_import' );

        }

        public function admin_menu()
        {

            // add parent item
            add_menu_page(
                __('SimpleView', 'acfsv'),
                __('SimpleView', 'acfsv'),
                'manage_options',
                'acfsv-admin',
                array($this, 'acfsv_import_page')
            );

            add_submenu_page(
                'acfsv-admin',
                __('Listings', 'acfsv'),
                __('Listings', 'acfsv'),
                'manage_options',
                'acfsv-admin',
                array($this, 'acfsv_import_page')
            );
            add_submenu_page(
                'acfsv-admin',
                __('Forms', 'acfsv'),
                __('Forms', 'acfsv'),
                'manage_options',
                'form-page',
                array($this,'acfsv_form_page')
            );
        }

        public function acfsv_import_page()
        {
            include_once ACFSV_PATH . '/admin/admin-listings.php';
        }

        public function acfsv_form_page()
        {
            include_once ACFSV_PATH . '/admin/admin-forms.php';
        }

        public function sv_import_data($listingId=0)
        {

            if ($listingId > 0) {

                $results = ACFSV_getListing($listingId);

            } else {

                $results = ACFSV_getAllListings();

            }

        
        }
        public function sv_import_data_test($listingId=0)
        {

            if ($listingId > 0) {

                $results = ACFSV_getListing($listingId);
                print_r($results);
                return;

            } else {

                $results = ACFSV_getAllListings();
                return $results;
    
            }
        
        }
    }

    $acfsv_admin = new ACFSV_admin();
    
}

function acfsv_admin_listing_import() {
    $listingId = $_REQUEST['sv'];
    $runType = $_REQUEST['type'];

    switch($runType) {
        case 'test':

            if ($listingId > 0) {
                $results = ACFSV_getListing(null, $listingId, 'json');
                print "<pre>" . $results . "</pre>";
                return;
            }
            break;
        default:
            if ($listingId > 0) {
                $results = ACFSV_getListing(null, $listingId, 'not');
                ACFSV_createListingPost($results);
                return;
            }
            
            ACFSV_getAllListings();
    }

}