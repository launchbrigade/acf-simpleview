<?php

?>
<style>
    /* webkit css bugfix: http://css-tricks.com/8439-webkit-sibling-bug/ */
    body {
        -webkit-animation: bugfix infinite 1s;
    }

    @-webkit-keyframes bugfix {
        from {
            padding: 0;
        }

        to {
            padding: 0;
        }
    }

    /* end of bugfix */
    /* layout&functionality */

    section {
        display: flex;
        justify-content: space-around;
        position: relative;
        padding: 1.5rem;
        font-size: 1rem;
    }

    .row {
        display: flex;
        justify-content: space-between;
        align-content: space-between;
    }

    .column {
        width: 48%;
    }

    button,
    input[type="submit"] {
        padding: .25rem 1.25rem;
        background-color: #ffffff;
        box-shadow: 0 3px 9px rgba(0, 0, 0, .2);
        border: 0;
        transition: box-shadow .25s ease-in-out;
    }

    button:hover,
    input[type="submit"]:hover {
        box-shadow: 0 5px 15px rgba(0, 0, 0, .2);
    }

    button:active,
    input[type="submit"]:active {
        box-shadow: 0 2px 6px rgba(0, 0, 0, .2);
    }

    button.primary {
        background-color: #3d79d0;
        color: #ffffff;
    }

    p {
        padding: .5rem;
        font-size: inherit;
    }
</style>

<section id="acfsv-admin-head">
    <h2>
        <?php _e("SimpleView Forms", 'acfsv'); ?>
    </h2>
</section>
<section>
    <article class="accordion animated">
        <!-- view starts here -->
        <div class="view">
            <p>
                Show form ids from SimpleView
            </p>
            <p>
                <button class="primary">Show!</button>
            </p>
        </div>
        <!-- view ends here -->

        <!-- view starts here -->
        <div class="view">

            <p>
                Put in a listing ID from business listings to import
            </p>
        </div>
        <!-- view ends here -->

    </article>
</section>