<?php

?>
<style>
    /* webkit css bugfix: http://css-tricks.com/8439-webkit-sibling-bug/ */
body { -webkit-animation: bugfix infinite 1s; }
@-webkit-keyframes bugfix { from { padding: 0; } to { padding: 0; } }
/* end of bugfix */
/* layout&functionality */
.accordion>.view>input.toggle
{
    position: absolute;
    display: block;
    width: 100%;
    height: 32px;
    opacity: 0;
    z-index: 1;
    cursor: pointer;
}
.accordion>.view>input.toggle:not(:checked)~.content
{
    display: none;
}
    /* special treat for ios */
    .accordion>.view>input.toggle:checked~.content
    {
        display: block;
    }
.accordion>.view>input.toggle~.header>label
{
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
}
/* look&feel */
.accordion
{
    position: relative;
    display: inline-block;
    width: 80vw;
    max-width:350px;
    margin-left: 60px;

    background-color: #ffffff;
    box-shadow: 0 5px 15px rgba(0,0,0,.2);
}
    .accordion:first-child
    {
        margin-left: 0px;
    }
.accordion:before
{
    content: "";

    position: absolute;
    top: -1px;
    right: -1px;
    bottom: -1px;
    left: -1px;
    width: auto;
    height: auto;
    z-index: -1;
}
.accordion>.view>.header
{
    position: relative;
    padding: 5px 10px;
    color: #3d79d0;
    /* text-shadow: 0px 1px 1px #3d79d0; */
    font-size: 1.25rem;
    line-height: 1.6;
    border-top: 1px solid rgba(0,0,0,.12);

    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
}
    .accordion>.view>input.toggle:checked~.header
    {
        color: #fff;
        background-color: #3d79d0;
    }
    .accordion>.view>input.toggle:hover~.header,
    .accordion>.view>.header:hover
    {
        color: #3b77d8;
        /* text-shadow:  0px 1px 1px #ffffff, 0px 0px 1px #3d79d0; */
    }
    .accordion>.view>input.toggle:checked:hover~.header,
    .accordion>.view>input.toggle:checked~.header:hover
    {
        color: #eee;
        /* text-shadow:  0px 1px 1px #ffffff, 0px 0px 1px #3d79d0; */
    }
    /* .accordion>.view>input.toggle:active~.header,
    .accordion>.view>.header:active
    {
        background-color: #ccc;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#c3c3c3), to(#ebebeb));
        background-image: -moz-linear-gradient(top, #c3c3c3, #ebebeb);
        border-color: #a6a6a6;
        border-top-color: #939393;
        border-bottom-color: #bcbcbc;
    } */
.accordion>.view>.header>label
{
    display: block;
    width: 100%;
}
.accordion>.view>.header>.arrow
{
    position: absolute;
    display: block;
    top: 1.25rem;
    right: 8px;
    width: 0px;
    height: 0px;
    border-width: 4px;
    border-style: solid;
    border-color: transparent transparent transparent #999999;

    -webkit-transition: border-color 0.2s ease-in-out;
    -moz-transition: border-color 0.2s ease-in-out;
    transition: border-color 0.2s ease-in-out;
}
    .accordion>.view>input.toggle:checked~.header>.arrow
    {
        top: 1.25rem;
        right: 10px;
        border-color: #fff transparent transparent transparent;
    }
    .accordion>.view>input.toggle:hover~.header>.arrow,
    .accordion>.view>.header:hover>.arrow
    {
        border-color: transparent transparent transparent #3d79d0;
    }
        .accordion>.view>input.toggle:checked:hover~.header>.arrow,
        .accordion>.view>input.toggle:checked~.header:hover>.arrow
        {
            border-color: #fff transparent transparent transparent;
        }
.accordion>.view>.content
{
    margin-bottom: 5px;
    padding: 5px 10px;
}
.accordion>.view>.content>p
{
    margin: 0px;
    /* text-align: justify; */
}
/* massage content */
.accordion:after
{
    content: "";

    position: absolute;
    top: -30px;
    right: 0px;
    left: 10px;
    width: auto;
    height: 30px;
    line-height: 30px;
    z-index: -1;
}
ul>li:hover
{
    color: #3d79d0;

    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
}
.accordion>.view>.content.picture
{
    text-align: center;
}
.accordion img.sample
{
    width: 240px;
    height: 240px;
    border: 1px solid #999999;
    -webkit-box-shadow: 1px 1px 1px #ffffff;
    -moz-box-shadow: 1px 1px 1px #ffffff;
    box-shadow: 1px 1px 1px #ffffff;
}
/*
 * Here goes firefox only stuff
 */
@-moz-document url-prefix()
{
/* add your ff-only stuff below */
    .accordion>.view>.header>.arrow
    {
        top: 1.25rem;
        left: auto;
    }
        .accordion>.view>input.toggle:checked~.header>.arrow
        {
            top: 1.25rem;
        }
/* add your ff-only stuff above */
}
/* more nice to have hacks */
@media all and (orientation:landscape)
{
    /* consider it as default */
}
@media all and (orientation: portrait)
{
    .accordion
    {
        display: block;
        margin: 60px auto 30px;
    }
        .accordion:first-child
        {
            margin: auto auto 30px;
        }
}
@media all and (max-width: 1020px)
{
    .accordion
    {
        display: block;
        margin: 60px auto 30px;
    }
        .accordion:first-child
        {
            margin: auto auto 30px;
        }
}
/* couldn't come up with less hacky way, leave like that for now */
/* animation */
.accordion.animated>.view>input.toggle:not(:checked)~.content
{
    display: block;
    visibility: hidden;
    height: 0px;
    margin-bottom: 0px;
    padding: 0px 10px;
}
    .accordion.animated>.view>input.toggle:checked~.content
    {
        display: block;
        visibility: visible;
        /* height: auto; */
        margin-bottom: 5px;
        padding: 5px 10px;
    }
/* 100px */
.accordion.animated>.view>input.toggle:not(:checked)~.content.animated100
{
    overflow: hidden;
    -webkit-animation-name: disappearing100;
    -webkit-animation-duration: 1s;
    -moz-animation-name: disappearing100;
    -moz-animation-duration: 1s;
}
.accordion.animated>.view>input.toggle:checked~.content.animated100
{
    overflow: hidden;
    -webkit-animation-name: appearing100;
    -webkit-animation-duration: 1s;
    -moz-animation-name: appearing100;
    -moz-animation-duration: 1s;
}
/* 120px */
.accordion.animated>.view>input.toggle:not(:checked)~.content.animated120
{
    overflow: hidden;
    -webkit-animation-name: disappearing120;
    -webkit-animation-duration: 1s;
    -moz-animation-name: disappearing120;
    -moz-animation-duration: 1s;
}
.accordion.animated>.view>input.toggle:checked~.content.animated120
{
    overflow: hidden;
    -webkit-animation-name: appearing120;
    -webkit-animation-duration: 1s;
    -moz-animation-name: appearing120;
    -moz-animation-duration: 1s;
}
/* 240px */
.accordion.animated>.view>input.toggle:not(:checked)~.content.animated240
{
    overflow: hidden;
    -webkit-animation-name: disappearing240;
    -webkit-animation-duration: 1s;
    -moz-animation-name: disappearing240;
    -moz-animation-duration: 1s;
}
.accordion.animated>.view>input.toggle:checked~.content.animated240
{
    overflow: hidden;
    -webkit-animation-name: appearing240;
    -webkit-animation-duration: 1s;
    -moz-animation-name: appearing240;
    -moz-animation-duration: 1s;
}
.accordion.animated>.view>input.toggle:checked~.content.animated400
{
    /* overflow: hidden; */
    /* max-height: 400px; */
    /* transition: all 1s; */
    -webkit-transition: all 2s ease-in-out;
    -moz-transition: all 2s ease-in-out;
    transition: all 2s ease-in-out;
}
/* keyframes */
/* since height: auto doesn't produce good animation, hardcode for now */
/* height 100px */
@-webkit-keyframes disappearing100
{
    0%
    {
        visibility: visible;
        height: 100px;
        padding: 5px 10px;
    }
    100%
    {
        visibility: visible;
        height: 0px;
        padding: 0px 10px;
    }
}
@-moz-keyframes disappearing100
{
    0%
    {
        visibility: visible;
        height: 100px;
    }
    100%
    {
        visibility: visible;
        height: 0px;
    }
}
@-webkit-keyframes appearing100
{
    0%
    {
        visibility: visible;
        height: 0px;
    }
    100%
    {
        visibility: visible;
        height: 100px;
    }
}
@-moz-keyframes appearing100
{
    0%
    {
        visibility: visible;
        height: 0px;
    }
    100%
    {
        visibility: visible;
        height: 100px;
    }
}
/* height 120px */
@-webkit-keyframes disappearing120
{
    0%
    {
        visibility: visible;
        height: 120px;
    }
    100%
    {
        visibility: visible;
        height: 0px;
    }
}
@-moz-keyframes disappearing120
{
    0%
    {
        visibility: visible;
        height: 120px;
    }
    100%
    {
        visibility: visible;
        height: 0px;
    }
}
@-webkit-keyframes appearing120
{
    0%
    {
        visibility: visible;
        height: 0px;
    }
    100%
    {
        visibility: visible;
        height: 120px;
    }
}
@-moz-keyframes appearing120
{
    0%
    {
        visibility: visible;
        height: 0px;
    }
    100%
    {
        visibility: visible;
        height: 120px;
    }
}
/* height 240px */
@-webkit-keyframes disappearing240
{
    0%
    {
        visibility: visible;
        height: 240px;
    }
    100%
    {
        visibility: visible;
        height: 0px;
    }
}
@-moz-keyframes disappearing240
{
    0%
    {
        visibility: visible;
        height: 240px;
    }
    100%
    {
        visibility: visible;
        height: 0px;
    }
}
@-webkit-keyframes appearing240
{
    0%
    {
        visibility: visible;
        height: 0px;
    }
    100%
    {
        visibility: visible;
        height: 240px;
    }
}
@-moz-keyframes appearing240
{
    0%
    {
        visibility: visible;
        height: 0px;
    }
    100%
    {
        visibility: visible;
        height: 240px;
    }
}

section {
    display: flex;
    justify-content: space-around;
    position: relative;
    padding: 1.5rem;
    font-size: 1rem;
}
.row {
    display: flex;
    justify-content: space-between;
    align-content: space-between;
}
.column {
    width: 48%;
}
button, 
input[type="submit"] {
    padding: .25rem 1.25rem;
    background-color: #ffffff;
    box-shadow: 0 3px 9px rgba(0,0,0,.2);
    border: 0;
    transition: box-shadow .25s ease-in-out;
    margin-right: .5rem;
}
button:hover, 
input[type="submit"]:hover {
    box-shadow: 0 5px 15px rgba(0,0,0,.2);
}
button:active, 
input[type="submit"]:active {
    box-shadow: 0 2px 6px rgba(0,0,0,.2);
}
button.primary {
    background-color: #3d79d0;
    color: #ffffff;
}
p { padding: .5rem; font-size: inherit; }
</style>

<section id="acfsv-admin-head">
    <h2>
        <?php _e("SimpleView Listings", 'acfsv'); ?>
    </h2>
</section>
<section>
    <article class="accordion animated">
        <?php 
            // print ini_get('max_execution_time') . "<br/>"; 
            // print ini_get('upload_max_filesize') . "<br/>";
            // print (!ini_get('safe_mode')) ? "Not Safe Mode<br/>" : "Safe Mode<br/>";
        ?>
        <!-- view starts here -->
        <div class="view">

            <input id="accordion_animated_1" class="toggle" type="radio" checked="checked" name="toggle_animated" value="">
            <div class="header"><label for="accordion_animated_1">
                    <?php _e("Import all listings", 'acfsv'); ?></label><span class="arrow"></span></div>
            <div class="content">
                <p>
                    <?php _e("Import all business listings from SimpleView", 'acfsv'); ?>
                </p>
                <p>
                    <button class="primary" data-listings="all">
                        <?php _e("Start!", 'acfsv'); ?></button>
                    <button class="test" data-listings="all-test">
                        <?php _e("Test", 'acfsv'); ?></button>
                </p>
                <p>
                    <?php _e("It is a good practice to test before you import your information.", 'acfsv'); ?>
                </p>
            </div>

        </div>
        <!-- view ends here -->

        <!-- view starts here -->
        <div class="view">

            <input id="accordion_animated_2" class="toggle" type="radio" name="toggle_animated" value="">
            <div class="header"><label for="accordion_animated_2">
                    <?php echo _e("Import single listing", 'acfsv'); ?></label><span class="arrow"></span></div>
            <div class="content">
                <p>
                    <?php echo _e("Put in a listing ID from business listings to import", 'acfsv'); ?>
                </p>
                <p class="row">
                    <label class="column">
                        <?php echo _e("Listing Id", 'acfsv'); ?></label>
                    <input id="listingid" class="column" type="text" name="listingid">
                </p>
                <p>
                    <button class="primary" data-listings="single">
                        <?php _e("Start!", 'acfsv'); ?></button>
                    <button class="test" data-listings="single-test">
                        <?php _e("Test", 'acfsv'); ?></button>
                </p>
                <p>
                    <?php _e("It is a good practice to test before you import your information.", 'acfsv'); ?>
                </p>
            </div>

        </div>
        <!-- view ends here -->

    </article>
</section>
<section>
    <article>
        <h2></h2>
        <pre data-listings-output>
        </pre>
    </article>
</section>
<script>
    (function ($) {
        $(document).ready(function () {
            $('[data-listings]').click(function () {
                let listings = $(this).data('listings')
                let listingVal = $('#listingid').val()

                switch (listings) {
                    case 'all':
                        importAll();
                        break
                    case 'all-test':
                        importAllTest()
                        break
                    case 'single':
                        importSingle(listingVal)
                        break
                    case 'single-test':
                        importSingleTest(listingVal)
                        break
                }
            });

        });

        // Trigger SimpleView Import
        function importAll() {

            let form = document.createElement('form');
            form.action = '/wp-admin/admin-post.php';
            form.method = 'POST';

            form.innerHTML = '<input type="hidden" name="action" value="listing_import">' +
                '<input type="hidden" name="sv" value="0">' +
                '<input type="hidden" name="type" value="nonce">'

            // the form must be in the document to submit it
            document.body.append(form)

            form.submit();

        }

        // Trigger SimpleView Import Test
        function importAllTest() {

            let form = document.createElement('form');
            form.action = '/wp-admin/admin-post.php';
            form.method = 'POST';

            form.innerHTML = '<input type="hidden" name="action" value="listing_import">' +
                '<input type="hidden" name="sv" value="0">' +
                '<input type="hidden" name="type" value="test">'

            // the form must be in the document to submit it
            document.body.append(form)

            form.submit();

        }

        // Trigger SimpleView Single Import
        function importSingle(id) {

            let form = document.createElement('form');
            form.action = '/wp-admin/admin-post.php';
            form.method = 'POST';

            form.innerHTML = '<input type="hidden" name="action" value="listing_import">' +
                '<input type="hidden" name="sv" value="' + id + '">' +
                '<input type="hidden" name="type" value="nonce">'

            // the form must be in the document to submit it
            document.body.append(form)

            form.submit();

        }

        // Trigger SimpleView Simple Import Test
        function importSingleTest(id) {
            // alert('import single test : ' + id)
            let form = document.createElement('form');
            form.action = '/wp-admin/admin-post.php';
            form.method = 'POST';

            form.innerHTML = '<input type="hidden" name="action" value="listing_import">' +
                '<input type="hidden" name="sv" value="' + id + '">' +
                '<input type="hidden" name="type" value="test">'

            // the form must be in the document to submit it
            document.body.append(form);

            form.submit();
        }
    })(jQuery);
</script>

<?php
// print "<pre>";
// ACFSV_getAllListings();
// print_r(ACFSV_importListings());
// print "</pre>";
// print "<pre>";
// print_r(Acfsv_getListingPosts());
// print "</pre>";