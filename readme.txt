=== Advanced Custom Fields - SimpleView ===
Contributors: Darren <darren@odden.io> @ Launch Brigade
Tags: acf, advanced, custom, field, fields, form, repeater, content
Requires at least: 5.0.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Connects SimpleView with Advanced Custom Fields use in WordPress

== Description ==

Use the Advanced Custom Fields SimpleView plugin to provide a simple connect to import SimpleView into your web data

== GIT Submodule for SimpleView-PHP ==

```
git submodule init
git submodule update
```

#### As of Git 1.8.2 new option --remote was added

```
git submodule update --remote --merge
```

will fetch the latest changes from upstream in each submodule, merge them in, and check out the latest revision of the submodule.