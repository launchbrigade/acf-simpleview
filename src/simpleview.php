<?php
/**
 * Get the data from SimpleView
 */

require_once ACFSV_PATH . "/vendor/simpleview-php/src/simpleViewLibrary.php";
require_once ACFSV_PATH . "/vendor/simpleview-php/src/simpleViewFilter.php";

use SimpleView\SimpleViewLibrary;
use SimpleView\SimpleViewFilter;

if (!function_exists('ACFSV_getConfig')) {
    /**
     * Undocumented function
     *
     * @return object
     */
    function ACFSV_getConfig()
    {
        $jsonStr = file_get_contents(ACFSV_CONFIG);
        return json_decode($jsonStr);
    }
}

if (!function_exists('ACFSV_create')) {
    /**
     * Create the soap connection
     *
     * @return object SimpleView SOAP connection
     */
    function ACFSV_create()
    {
        try {
            $config = ACFSV_getConfig();
            return new SimpleViewLibrary($config);
        } catch (SoapFault $error) {
            try {
                $config = ACFSV_getConfig();
                return new SimpleViewLibrary($config);
            } catch (SoapFault $error) {
                trigger_error("SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})", E_USER_ERROR);
            }
        }
    }
}

if (!function_exists('ACFSV_filterAll')) {
    /**
     * Filter all SimpleView
     *
     * @return object all listings filter
     */
    function ACFSV_filterAll()
    {
        $simpleViewFilter = new SimpleViewFilter(
            $config->filter->fieldCategory,
            $config->filter->fieldName,
            $config->filter->filterType,
            $config->filter->filterValue
        );

        $allListings = $simpleViewFilter->filterAllListings();

        return $allListings;
    }
}

if (!function_exists('ACFSV_getListingIds')) {
    /**
     * Undocumented function
     *
     * @param object  $connection simpleview connection object
     * @param object  $filter     filter object
     * @param integer $pageSize   how many results per page to return
     *
     * @return void
     */
    function ACFSV_getListingIds($connection, $filter, $pageSize)
    {
        try {
            $resultNumber = ACFSV_getResultsNumber($connection, $filter, 0);
        } catch (SoapFault $error) {
            try {
                $connection = ACFSV_create();
                $filter = ACFSV_filterAll();
                $resultNumber = ACFSV_getResultsNumber($connection, $filter, 0);
            } catch (SoapFault $error) {
                trigger_error("SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})", E_USER_ERROR);
            }
        }

        $pages = $resultNumber % $pageSize;
        $results = array();

        for ($page = 1; $page<=$pages; $page++) {
            try {
                $listings = $connection->getListings($pageSize, $page, $filter, 0);
            } catch (SoapFault $error) {
                try {
                    $connection = ACFSV_create();
                    $listings = $connection->getListings($pageSize, $page, $filter, 0);
                } catch (SoapFault $error) {
                    trigger_error("SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})", E_USER_ERROR);
                }
            }

            foreach ($listings["DATA"] as $listing) {
                array_push($results, $listing["LISTINGID"]);
            }
        }
        return $results;
    }
}

if (!function_exists('ACFSV_getResultsNumber')) {
    /**
     * Get the number of results to process
     *
     * @param object $connection    SOAP connection
     * @param array  $filter        Array of filterAllListings
     * @param bool   $showAmenities True or False
     *
     * @return integer number of results
     */
    function ACFSV_getResultsNumber(
        $connection,
        $filter,
        $showAmenities
    ) {
        try {
            $initial = $connection->getListings(1, 1, $filter, $showAmenities);
        } catch (SoapFault $error) {
            try {
                $connection = ACFSV_create();
                $initial = $connection->getListings(1, 1, $filter, $showAmenities);
            } catch (SoapFault $error) {
                trigger_error("SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})", E_USER_ERROR);
                return;
            }
        }
        return $initial['STATUS']['RESULTS'];
    }
}


if (!function_exists('ACFSV_getAllListings')) {
    // TODO: write the function doc
    function ACFSV_getAllListings()
    {
        $svl = ACFSV_create();

        $filter = ACFSV_filterAll();

        try {
            $listingIds = ACFSV_getListingIds($svl, $filter, 100);
        } catch (SoapFault $error) {
            try {
                $svl = ACFSV_create();
                $filter = ACFSV_filterAll();


                $listingIds = ACFSV_getListingIds($svl, $filter, 100);
            } catch (SoapFault $error) {
                wp_mail(
                    array("darren@odden.io","scadmin@launchbrigade.com"),
                    "SimpleView Import Get Ids",
                    "SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})"
                );
                trigger_error("SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})", E_USER_ERROR);
            }
        }

        ACFSV_importListings($svl, $listingIds);

        return;
    }
}

if (!function_exists('ACFSV_importListings')) {
    function ACFSV_importListings($svl, $listingIds)
    {
        if (!isset($svl)) {
            try {
                $svl = ACFSV_create();
            } catch (SoapFault $fault) {
                try {
                    $svl = ACFSV_create();
                } catch (SoapFault $fault) {
                    trigger_error("SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})", E_USER_ERROR);
                }
            }
        }
        if (count($listingIds) > 0) {
            foreach ($listingIds as $listingId) {
                print "-----< list id : " . $listingId . " >-----<br/>\n";
                $listingPosts = ACFSV_getPostIdFromListingId($listingId);
                 print_r($listingPosts);
                if ($listingPosts) {
                    foreach ($listingPosts as $listingPost) {
                        print "-----< post id : " . $listingPost->ID . " | modified : ". $listingPost->post_modified . " | listing updated : ". $listing["LASTUPDATED"] . " >-----<br/>\n";

                        $listing = ACFSV_getListing($svl, $listingId, 'not');
                        if (strtotime($listingPost->post_modified) < strtotime($listing["LASTUPDATED"])) {
                            ACFSV_updateListingPost($listingPost->ID, $listing);
                        }
                    }
                } else {
                    try {
                        $listing = ACFSV_getListing($svl, $listingId, 'not');
                    } catch (SoapFault $error) {
                        try {
                            $svl = ACFSV_create();
                            $listing = ACFSV_getListing($svl, $listingId, 'not');
                        } catch (SoapFault $error) {
                            wp_mail(
                                array("darren@odden.io","scadmin@launchbrigade.com"),
                                "SimpleView Import Get Listings",
                                "SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})"
                            );

                            trigger_error("SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})", E_USER_ERROR);
                        }
                    }

                    ACFSV_createListingPost($listing);
                }
            }
        }
    }
}

if (!function_exists('ACFSV_getPostIdFromListingId')) {
    function ACFSV_getPostIdFromListingId($listingId)
    {
        print "---- getPostIdFromListingId ----<br/>\n";
        // ListingId Key :: field_5c094c1e03a13
        $listings_args = array(
            'post_type' => 'listings',
            'meta_key' => 'listing_id',
            // 'meta_key' => 'field_5c094c1e03a13',
            'meta_value' => "{$listingId}",
        );
        $listings_query = null;
        $listings_query = new WP_Query($listings_args);
        if (count($listings_query->posts) > 0) {
              return $listings_query->posts;
        }

        return false;
    }
}

if (!function_exists('ACFSV_getListing')) {
    // TODO: write the function doc
    function ACFSV_getListing($svl, $listingId, $type)
    {
        try {
            if (!isset($svl)) {
                $svl = ACFSV_create();
            }

            $listing = $svl->getListing($listingId, 0);
            //var_dump($listing);
        } catch (SoapFault $error) {
            try {
                if (!isset($svl)) {
                    $svl = ACFSV_create();
                }
                $listing = $svl->getListing($listingId, 0);
            } catch (SoapFault $error) {
                wp_mail(
                    array("rreyes@launchbrigade.com","scadmin@launchbrigade.com"),
                    "SimpleView Import Get Listing",
                    "SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})"
                );

                trigger_error("SoapFault: (faultcode:{$error->faultcode}, faultstring: {$error->faultstring})", E_USER_ERROR);
            }
        }
        switch ($type) {
            case 'json':
                return json_encode($listing["DATA"], JSON_PRETTY_PRINT);
            default:
                return $listing["DATA"];
        }
    }
}

if (!function_exists('ACFSV_createListingPost')) {
    function ACFSV_createListingPost($listing)
    {
        if (isset($listing) && is_array($listing)) {
            $listingPost = ACFSV_getPostIdFromListingId($listing["LISTINGID"]);
            if ($listingPost) {
                $post_id = $listingPost[0]->ID;
                print "post_id : " . $post_id . "<br/>";
            }
        }

        if (!isset($post_id)) {
            $newPost = array();
            $newPost['post_title'] = str_replace("&amp;", "&", $listing["SORTCOMPANY"]);
            $newPost['post_title'] = str_replace("&amp;", "&", $listing["SORTCOMPANY"]);
            $newPost['post_content'] = str_replace("&amp;", "&", $listing["DESCRIPTION"]);
            $newPost['post_status'] = 'publish';
            $newPost['author']      = 49; // user scadmin
            $newPost['post_type']   = 'listings';
            $newPost['comment_status'] = 'closed';

            $post_id = wp_insert_post($newPost);
            print "This is a new post\npost_id : " . $post_id . "<br/>";
        }

        ACFSV_updateListingPost($post_id, $listing);
    }
}

if (!function_exists('ACFSV_updateListingPost')) {
    function ACFSV_updateListingPost($post_id, $listing)
    {
        $post = get_post($post_id);
        if (!isset($post)) {
            return false;
        }

        $update_post['ID'] = $post_id;

        if ($post->post_title != str_replace("&amp;", "&", $listing["SORTCOMPANY"])) {
            $update_post['post_title'] = str_replace("&amp;", "&", $listing["SORTCOMPANY"]);
        }
        if ($post->post_content != str_replace("&amp;", "&", $listing["DESCRIPTION"])) {
            $update_post['post_content'] = str_replace("&amp;", "&", $listing["DESCRIPTION"]);
        }

        if (count($update_post) > 1) {
            wp_update_post($update_post);
        }

        print "post_id : " . $post_id . " : " . $listing["LISTINGID"] . " : " . $listing["SORTCOMPANY"] . "<br/>";
        //var_dump($listing["AMENITIES"]);

        (isset($listing["LISTINGID"])) ? update_field('field_5c094c1e03a13', $listing["LISTINGID"], $post_id) : '';
        (isset($listing["TYPENAME"]))  ? update_field('field_5c094c6003a14', $listing["TYPENAME"], $post_id) : '';
        (isset($listing["LATITUDE"]))  ? update_field('field_5bd78a2e9aec0', $listing["LATITUDE"], $post_id) : '';
        (isset($listing["LONGITUDE"])) ? update_field('field_5bd78a449aec1', $listing["LONGITUDE"], $post_id) : '';

        (isset($listing["CITY"]))      ? update_field('field_5c58a9e462a71', $listing["CITY"], $post_id) : '';
        (isset($listing["STATE"]))     ? update_field('field_5bd0a381aa76c', $listing["STATE"], $post_id) : '';
        (isset($listing["ZIP"]))       ? update_field('field_5c58a9f962a72', $listing["ZIP"], $post_id) : '';

        (isset($listing["ADDR1"]))     ? update_field('field_5bd0a356aa76b', $listing["ADDR1"], $post_id) : '';
        (isset($listing["REGION"]))    ? update_field('field_5c390fa4ca2dd', $listing["REGION"], $post_id) : '';
        (isset($listing["PHONE"]))     ? update_field('field_5bd0a3beaa76e', $listing["PHONE"], $post_id) : '';
        (isset($listing["FAX"]))       ? update_field('field_5c33a77b2e4a6', $listing["FAX"], $post_id) : '';
        (isset($listing["TOLLFREE"]))  ? update_field('field_5c33a77b2e4a6', $listing["TOLLFREE"], $post_id) : '';
        (isset($listing["WEBURL"]))    ? update_field('field_5bd0a3c9aa76f', $listing["WEBURL"], $post_id) : '';

        (isset($listing["CATNAME"]))   ? update_field('field_5c2f7eef729fe', $listing["CATNAME"], $post_id) : '';
        (isset($listing["EMAIL"]))     ? update_field('field_5c094ca603a16', $listing["EMAIL"], $post_id) : '';
        (isset($listing["PRIMARYCONTACTFULLNAME"])) ? update_field('field_5c094cbd03a17', $listing["PRIMARYCONTACTFULLNAME"], $post_id) : '';
 
        (isset($listing["LASTUPDATED"]))    ? update_field('field_5ef386f1544e5', $listing["LASTUPDATED"], $post_id) : '';

        if (isset($listing["IMAGES"]) && count($listing["IMAGES"]) > 0) {
            foreach ($listing["IMAGES"] as $image) {
                $image_url[] = array('field_5c531cb6daae0' => $image["IMGPATH"] . $image["MEDIAFILE"]);
            }
            update_field('field_5c531ca6daadf', $image_url, $post_id);
        }

        // SOCIAL MEDIA
        if (isset($listing["SOCIALMEDIA"]) && count($listing["SOCIALMEDIA"]) > 0) {
            foreach ($listing["SOCIALMEDIA"] as $socialplatform) {
                $social_fields[] = array(
                    'field_5c094d1303a1a' => $socialplatform["SERVICE"],
                    'field_5c094d2e03a1b' => $socialplatform["VALUE"]
                );
            }
            update_field('field_5c094d0503a19', $social_fields, $post_id);
        }

        $categories = $listing["CATEGORYLIST"];

        // SUBCATEGORIES

        $subcategories = array($listing["SUBCATNAME"]);
        if (isset($listing["ADDITIONALSUBCATS"]) && count($listing["ADDITIONALSUBCATS"]) > 0) {
            foreach ($listing["ADDITIONALSUBCATS"] as $subcategory) {
                array_push($subcategories, $subcategory["SUBCATNAME"]);
            }
        }

        // AMENITIES
        $amenities = array();
        $amenities = amenities($listing["AMENITIES"]);

        //var_dump($amenities);

        $exclude_list = array("Aptos","Capitola","Davenport","San Lorenzo Valley","Santa Cruz","Scotts Valley","Watsonville","Pleasure Point","Santa Cruz City","Santa Cruz County");

        foreach($exclude_list as $key){
          if(array_key_exists($key,$amenities))
          update_field('field_5c390fa4ca2dd', $key, $post_id);
          unset($amenities[$key]);
        }

        //(isset($amenities["Beach"]))           ? update_field('field_5bd0a94f2008e', 'Beach', $post_id) : '';
        (isset($amenities["Beach"]))            ? update_field('field_5bd0a94f2008e', $amenities["Beach"], $post_id) : '';
        (isset($amenities["Town"]))            ? update_field('field_5bd0a94f2008e', $amenities["Town"], $post_id) : '';
        (isset($amenities["Rural"]))           ? update_field('field_5bd0a94f2008e', $amenities["Rural"], $post_id) : '';
        (isset($amenities["Mountains"]))       ? update_field('field_5bd0a94f2008e', $amenities["Mountains"], $post_id) : '';

        unset($amenities["Beach"], $amenities["Town"], $amenities["Rural"], $amenities["Mountains"]);

        // field_5c587a7e58454
        (isset($amenities["Fav"]) || isset($amenities["FAV"])) ? update_field('field_5c587a7e58454', true, $post_id) : update_field('field_5c587a7e58454', false, $post_id);

        (isset($amenities["Pets OK"]))                       ? update_field('field_5bd0d9825ed70', true, $post_id) : update_field('field_5bd0d9825ed70', false, $post_id);

        unset($amenities["Pets OK"]);

        if ($listing["TYPEID"] == 5 ) {
            $hours = str_replace("%", "<br>", $listing["AMENITIES"][71]["VALUE"]);
            $hours_field[] = array('field_5c2fd379c9202' => $hours);
            update_field('field_5c2fd343c9201', $hours_field, $post_id);
            unset($amenities['Days / Hours of Business']);
        }
        elseif (isset($amenities['Days / Hours of Business'])) {
            $hours = str_replace("%", "<br>", $amenities['Days / Hours of Business']);
            $hours_field[] = array('field_5c2fd379c9202' => $hours);
            update_field('field_5c2fd343c9201', $hours_field, $post_id);
            unset($amenities['Days / Hours of Business']);
        }

        //COVID
        (isset($amenities["CDC Recommended Cleaning Procedures"])) ? update_field('field_5eecef289d6e7', true, $post_id) : update_field('field_5eecef289d6e7', false, $post_id);
        (isset($amenities["COVID Open For Business"])) ? update_field('field_5eecef4e9d6e8', true, $post_id) : update_field('field_5eecef4e9d6e8', false, $post_id);
        (isset($amenities["Delivery Available"])) ? update_field('field_5eecef689d6e9', true, $post_id) : update_field('field_5eecef689d6e9', false, $post_id);
        (isset($amenities["Health & Safety Policy File Upload"])) ? update_field('field_5eecefb09d6ea', $amenities["Health & Safety Policy File Upload"], $post_id) : update_field('field_5eecefb09d6ea', '', $post_id);
        (isset($amenities["Hand Wash/Sanitizer Stations"])) ? update_field('field_5eecefcf9d6eb', true, $post_id) : update_field('field_5eecefcf9d6eb', false, $post_id);
        (isset($amenities["Limited Elevator Occupancy"])) ? update_field('field_5eecefee9d6ec', true, $post_id) : update_field('field_5eecefee9d6ec', false, $post_id);
        (isset($amenities["Mobile Payments Accepted"])) ? update_field('field_5eecf00e9d6ed', true, $post_id) : update_field('field_5eecf00e9d6ed', false, $post_id);
        (isset($amenities["Mobile Payments Required"])) ? update_field('field_5eecf02b9d6ee', true, $post_id) : update_field('field_5eecf02b9d6ee', false, $post_id);
        (isset($amenities["No Contact Services"])) ? update_field('field_5eecf0479d6ef', true, $post_id) : update_field('field_5eecf0479d6ef', false, $post_id);
        (isset($amenities["Reduced Occupancy/Occupancy Limits"])) ? update_field('field_5eecf05f9d6f0', true, $post_id) : update_field('field_5eecf05f9d6f0', false, $post_id);
        (isset($amenities["Sneeze Guards/Contact Barriers"])) ? update_field('field_5eecf07c9d6f1', true, $post_id) : update_field('field_5eecf07c9d6f1', false, $post_id);
        (isset($amenities["Social Distance Furniture Configurations"])) ? update_field('field_5eecf08c9d6f2', true, $post_id) : update_field('field_5eecf08c9d6f2', false, $post_id);
        (isset($amenities["Social Distance Markings/Signage"])) ? update_field('field_5eecf0aa9d6f3', true, $post_id) : update_field('field_5eecf0aa9d6f3', false, $post_id);
        (isset($amenities["Social Distancing Guidelines Enforced"])) ? update_field('field_5eecf0be9d6f4', true, $post_id) : update_field('field_5eecf0be9d6f4', false, $post_id);
        (isset($amenities["Takeout Available"])) ? update_field('field_5eecf0d29d6f5', true, $post_id) : update_field('field_5eecf0d29d6f5', false, $post_id);
        (isset($amenities["Temporarily Closed"])) ? update_field('field_5eecf0dd9d6f6', true, $post_id) : update_field('field_5eecf0dd9d6f6', false, $post_id);
        (isset($amenities["Virtual Events"])) ? update_field('field_5eecf0e99d6f7', true, $post_id) : update_field('field_5eecf0e99d6f7', false, $post_id);
        (isset($amenities["Curbside Pick Up Available"])) ? update_field('field_5ef0ba222c9c9', true, $post_id) : update_field('field_5ef0ba222c9c9', false, $post_id);
        (isset($amenities["Drive Thru Available"])) ? update_field('field_5ef0ba332c9ca', true, $post_id) : update_field('field_5ef0ba332c9ca', false, $post_id);
        (isset($amenities["Online Orders Available"])) ? update_field('field_5ef0ba462c9cb', true, $post_id) : update_field('field_5ef0ba462c9cb', false, $post_id);
        (isset($amenities["Outdoor Dining"])) ? update_field('field_5ef0ba562c9cc', true, $post_id) : update_field('field_5ef0ba562c9cc', false, $post_id);
        (isset($amenities["Reservations Required"])) ? update_field('field_5ef0ba642c9cd', true, $post_id) : update_field('field_5ef0ba642c9cd', false, $post_id);
        (isset($amenities["Single Use Menus"])) ? update_field('field_5ef0ba792c9ce', true, $post_id) : update_field('field_5ef0ba792c9ce', false, $post_id);
        (isset($amenities["Table Dividers"])) ? update_field('field_5ef0ba892c9cf', true, $post_id) : update_field('field_5ef0ba892c9cf', false, $post_id);
        (isset($amenities["Takeout Available"])) ? update_field('field_5ef0ba9a2c9d0', true, $post_id) : update_field('field_5ef0ba9a2c9d0', false, $post_id);
        (isset($amenities["Delivery Service Available"])) ? update_field('field_5ef0baae2c9d1', true, $post_id) : update_field('field_5ef0baae2c9d1', false, $post_id);
        (isset($amenities["Masks Required for Dining Staff"])) ? update_field('field_5ef0babe2c9d2', true, $post_id) : update_field('field_5ef0babe2c9d2', false, $post_id);
        (isset($amenities["24 Hour Occupancy Hold"])) ? update_field('field_5ef0bada2c9d3', true, $post_id) : update_field('field_5ef0bada2c9d3', false, $post_id);
        (isset($amenities["Keyless Entry"])) ? update_field('field_5ef0baf62c9d4', true, $post_id) : update_field('field_5ef0baf62c9d4', false, $post_id);
        (isset($amenities["Mobile Concierge"])) ? update_field('field_5ef0bb0a2c9d5', true, $post_id) : update_field('field_5ef0bb0a2c9d5', false, $post_id);
        (isset($amenities["Social Distance Gym Configurations"])) ? update_field('field_5ef0bb182c9d6', true, $post_id) : update_field('field_5ef0bb182c9d6', false, $post_id);
        (isset($amenities["Community Health Virtual Classes"])) ? update_field('field_5ef0bb362c9d7', true, $post_id) : update_field('field_5ef0bb362c9d7', false, $post_id);
        (isset($amenities["Health Related Staff Training"])) ? update_field('field_5ef0bb552c9d8', true, $post_id) : update_field('field_5ef0bb552c9d8', false, $post_id);
        (isset($amenities["Health Screening Required-Guest"])) ? update_field('field_5ef0bb882c9d9', true, $post_id) : update_field('field_5ef0bb882c9d9', false, $post_id);
        (isset($amenities["Health Screening Required-Staff"])) ? update_field('field_5ef0bbb02c9da', true, $post_id) : update_field('field_5ef0bbb02c9da', false, $post_id);
        (isset($amenities["Masks Provided"])) ? update_field('field_5ef0bbc72c9db', true, $post_id) : update_field('field_5ef0bbc72c9db', false, $post_id);
        (isset($amenities["Masks Required (For Guests)"])) ? update_field('field_5ef0bbde2c9dc', true, $post_id) : update_field('field_5ef0bbde2c9dc', false, $post_id);
        (isset($amenities["Masks Required (For Staff)"])) ? update_field('field_5ef0bbf52c9dd', true, $post_id) : update_field('field_5ef0bbf52c9dd', false, $post_id);


        switch ($listing["TYPEID"]) {
            case '1': // Film
                $amenities = filteredamenities($listing["AMENITIES"], 5);
                (isset($amenities)) ? ACFSV_update_film($amenities, $categories, $subcategories, $post_id) : '';
                break;
            case '2': // Meetings
                $amenities = meetingamenities($listing["AMENITIES"], 4);
                (isset($amenities)) ? ACFSV_update_meeting($amenities, $subcategories, $listing["PROPERTYROOMS"], $post_id) : '';
                break;
            case '3': // Things To Do
                $amenities = filteredamenities($listing["AMENITIES"], 3);
                (isset($amenities)) ? ACFSV_update_things($subcategories, $categories, $post_id) : '';
                break;
            case '4': // Weddings
                $amenities = filteredamenities($listing["AMENITIES"], 4);
                (isset($amenities)) ? ACFSV_update_wedding($amenities, $subcategories, $categories, $listing["PROPERTYROOMS"][0]["BANQUET"], $post_id) : '';
                break;
            case '5': // Food & Drink
                $amenities = filteredamenities($listing["AMENITIES"], 2);
                (isset($amenities)) ? ACFSV_update_food($amenities, $subcategories, $categories, $post_id) : '';
                break;
            case '6': // Places To Stay
                $amenities = filteredamenities($listing["AMENITIES"], 1);
                (isset($amenities)) ? ACFSV_update_places($amenities, $subcategories, $post_id) : '';
                break;
        }
    }
}

function ACFSV_update_film($amenities, $category, $subcategories, $post_id)
{
    (isset($category)) ? update_field('field_5f46895a9ddf9', $category, $post_id) : '';
    (isset($subcategories)) ? update_field('field_5f4689709ddfa', $subcategories, $post_id) : '';
    (isset($amenities["Credits"])) ? update_field('field_5c33a932cc199', $amenities["Credits"], $post_id) : '';
}

function ACFSV_update_meeting($amenities, $subcategories, $property_rooms, $post_id)
{
    (isset($amenities)) ? update_field('field_5c3925713ce66', $amenities["Meeting Type"], $post_id) : '';

    if (isset($property_rooms) && count($property_rooms) > 0) {
        foreach ($property_rooms as $conferenceRoom) {
            $conference_fields[] = array(
                'field_5c3382d82ef2a' => $conferenceRoom["ROOMNAME"],
                'field_5c3382e62ef2b' => $conferenceRoom["SQFT"],
                'field_5c3382f32ef2c' => $conferenceRoom["WIDTH"]."x".$conferenceRoom["LENGTH"],
                'field_5c3383042ef2d' => $conferenceRoom["THEATER"],
                'field_5c33830f2ef2e' => $conferenceRoom["CLASSROOM"],
                'field_5c3383192ef2f' => $conferenceRoom["BANQUET"],
                'field_5c3383372ef30' => $conferenceRoom["RECEPTION"]
            );
        }

        update_field('field_5c3381c8f644c', $conference_fields, $post_id);
    }
    (isset($amenities["Number of On-Site Hotel Rooms"])) ? update_field('field_5c6b1fbb2b039', $amenities["Number of On-Site Hotel Rooms"], $post_id) : '';

    // unset used amenities
    unset($amenities["Dining Description"]);
    unset($amenities["Conference Description"]);
    unset($amenities["Retreat Description"]);
    unset($amenities["Wedding Description"]);
    unset($amenities["Wedding Service Description"]);
    unset($amenities["Accommodations Description"]);
    unset($amenities["Event Svc Description"]);
    unset($amenities["Actual URL"]);
    unset($amenities["Display URL"]);
    unset($amenities["Book It URL"]);
    unset($amenities["Check In Code"]);
    unset($amenities["Check Out Code"]);
    unset($amenities["Query Date Format"]);
    unset($amenities["Adult Guests Code"]);
    unset($amenities["Child Guests Code"]);
    unset($amenities["Number of On-Site Hotel Rooms"]);
    unset($amenities["Private Rooms"]);
    unset($amenities["Semi-Private Rooms"]);
    unset($amenities["With Private Bath"]);
    unset($amenities["Wedding Type"]);
    unset($amenities["Fav"]);
    unset($amenities["FAV"]);
    unset($amenities["Rate"]);
    unset($amenities["Book It"]);
    unset($amenities["Wedding Type"]);
    unset($amenities["Meeting Type"]);

    (isset($amenities)) ? update_field('field_5c337d018f94b', $amenities, $post_id) : '';
}

function ACFSV_update_things($subcategories, $categories, $post_id)
{
    (isset($categories)) ? update_field('field_5c2f7ec69b7ea', $categories, $post_id) : '';

    (isset($subcategories)) ? update_field('field_5c2fd246659b7', $subcategories, $post_id) : '';
}

function ACFSV_update_wedding($amenities, $subcategories, $categories, $venue_capacity, $post_id)
{
    isset($subcategories);
    $site = array();

    (isset($amenities)) ? update_field('field_5c3925713ce66', $amenities["Wedding Type"], $post_id) : '';

    (isset($amenities["Banquet/Events Facility"]))   ? array_push($site, $amenities["Banquet/Events Facility"]) : '';
    (isset($amenities["Bed and Breakfast"]))         ? array_push($site, $amenities["Bed and Breakfast"]) : '';
    (isset($amenities["Church/Chapel"]))             ? array_push($site, $amenities["Church/Chapel"]) : '';
    (isset($amenities["Conference Center"]))         ? array_push($site, $amenities["Conference Center"]) : '';
    (isset($amenities["Private Home/Estate"]))       ? array_push($site, $amenities["Private Home/Estate"]) : '';
    (isset($amenities["Hotel"]))                     ? array_push($site, $amenities["Hotel"]) : '';
    (isset($amenities["Inn/Lodge"]))                 ? array_push($site, $amenities["Inn/Lodge"]) : '';
    (isset($amenities["Park"]))                      ? array_push($site, $amenities["Park"]) : '';
    (isset($amenities["Rehearsal Dinners"]))         ? array_push($site, $amenities["Rehearsal Dinners"]) : '';
    (isset($amenities["Restaurant"]))                ? array_push($site, $amenities["Restaurant"]) : '';
    (isset($amenities["Retreat"]))                   ? array_push($site, $amenities["Retreat"]) : '';
    (isset($amenities["Winery"]))                    ? array_push($site, $amenities["Winery"]) : '';
    (isset($site)) ? update_field('field_5c2f7f7169ad9', $site, $post_id) : '';

    (isset($categories)) ? update_field('field_5c2f7eef729fe', $categories, $post_id) : '';

    $view = array();
    (isset($amenities["Country Setting"]))  ? array_push($view, $amenities["Country Setting"]) : '';
    (isset($amenities["Creek/River"]))      ? array_push($view, $amenities["Creek/River"]) : '';
    (isset($amenities["Fountain"]))         ? array_push($view, $amenities["Fountain"]) : '';
    (isset($amenities["Garden"]))           ? array_push($view, $amenities["Garden"]) : '';
    (isset($amenities["Patio/Courtyard"]))  ? array_push($view, $amenities["Patio/Courtyard"]) : '';
    (isset($amenities["Golf Course View"])) ? array_push($view, $amenities["Golf Course View"]) : '';
    (isset($amenities["Natural Setting"]))  ? array_push($view, $amenities["Natural Setting"]) : '';
    (isset($amenities["Ocean View"]))       ? array_push($view, $amenities["Ocean View"]) : '';
    (isset($amenities["Panoramic"]))        ? array_push($view, $amenities["Panoramic"]) : '';
    (isset($amenities["Park View"]))        ? array_push($view, $amenities["Park View"]) : '';
    (isset($amenities["Redwoods"]))         ? array_push($view, $amenities["Redwoods"]) : '';
    (isset($amenities["Vineyards"]))        ? array_push($view, $amenities["Vineyards"]) : '';
    (isset($amenities["No View"]))          ? array_push($view, $amenities["No View"]) : '';
    (isset($amenities["Yacht/Boat"]))       ? array_push($view, $amenities["Yacht/Boat"]) : '';
    (isset($view)) ? update_field('field_5c2f7fa369ada', $view, $post_id) : '';

    // Venue Capacity field_5c3927f71db0c
    (isset($venue_capacity)) ? update_field('field_5c3927f71db0c', $venue_capacity, $post_id) : '';
}

function ACFSV_update_food($amenities, $subcategories, $categories, $post_id)
{
    (isset($categories)) ? update_field('field_5c2f7e81113cd', $categories, $post_id) : '';

    (isset($subcategories)) ? update_field('field_5c2f7dd803de7', $subcategories, $post_id) : '';

    $signature_dish = array();
    (isset($amenities["Signature Menu Item 1"])) ? array_push($signature_dish, array('field_5c2e4089eb321' => $amenities["Signature Menu Item 1"])) : "";
    (isset($amenities["Signature Menu Item 2"])) ? array_push($signature_dish, array('field_5c2e4089eb321' => $amenities["Signature Menu Item 2"])) : "";
    (isset($amenities["Signature Menu Item 3"])) ? array_push($signature_dish, array('field_5c2e4089eb321' => $amenities["Signature Menu Item 3"])) : "";
    (isset($amenities["Signature Menu Item 4"])) ? array_push($signature_dish, array('field_5c2e4089eb321' => $amenities["Signature Menu Item 4"])) : "";

    (isset($signature_dish) && count($signature_dish) > 0) ? update_field('field_5c2e406deb320', $signature_dish, $post_id) : '';

    unset($amenities["Signature Menu Item 1"]);
    unset($amenities["Signature Menu Item 2"]);
    unset($amenities["Signature Menu Item 3"]);
    unset($amenities["Signature Menu Item 4"]);

    // Amenities
    // (isset($amenities)) ? update_field('field_5bd0a401aa772', $amenities, $post_id) : '';
}

function ACFSV_update_places($amenities, $subcategories, $post_id)
{
    // Type of Place field_5bd0ab3d9d7f1
    (isset($subcategories)) ? update_field('field_5bd0ab3d9d7f1', $subcategories, $post_id) : '';

    // Reservations
    (isset($amenities["Book It URL"])) ? update_field('field_5c09538bcccca', $amenities["Book It URL"], $post_id) : '';

    (isset($amenities["Pet-Friendly"])) ? update_field('field_5bd0d9825ed70', true, $post_id) : update_field('field_5bd0d9825ed70', false, $post_id);

    (isset($amenities["Check In Code"])) ? update_field('field_620c25d7932d1', $amenities["Check In Code"], $post_id) : '';
    (isset($amenities["Check Out Code"])) ? update_field('field_620c2629932d2', $amenities["Check Out Code"], $post_id) : '';
    (isset($amenities["Query Date Format"])) ? update_field('field_620c263c932d3', $amenities["Query Date Format"], $post_id) : '';
    (isset($amenities["Adult Guests Code"])) ? update_field('field_620c264a932d4', $amenities["Adult Guests Code"], $post_id) : '';
    (isset($amenities["Child Guests Code"])) ? update_field('field_620c2653932d5', $amenities["Child Guests Code"], $post_id) : '';


    // Number of On-Site Hotel Rooms
    (isset($amenities["Number of Units"]))               ? update_field('field_5c33c30307446', $amenities["Number of Units"], $post_id) : '';
    (isset($amenities["Number of On-Site Hotel Rooms"])) ? update_field('field_5c33c30307446', $amenities["Number of On-Site Hotel Rooms"], $post_id) : '';

    // Lodging Rates field_5c33c1c807443
    if (isset($amenities["Low Season"]) || isset($amenities["High Season"])) {
        $rate_fields['field_5c33c1d707444'] = $amenities["Low Season"];
        $rate_fields['field_5c33c1e607445'] = $amenities["High Season"];
        update_field('field_5c33c1c807443', $rate_fields, $post_id);
    }

    // unset used amenities
    unset($amenities["Book It"]);
    unset($amenities["Book It URL"]);
    unset($amenities["Number of Units"], $amenities["Number of On-Site Hotel Rooms"]);
    unset($amenities["Low Season"],      $amenities["High Season"]);
    unset($amenities["Accommodations Description"]);
    unset($amenities["Actual URL"]);
    unset($amenities["Display URL"]);
    unset($amenities["Price"]);
    unset($amenities["Fav"]);
    unset($amenities["FAV"]);
    unset($amenities["Pet-Friendly"]);
    unset($amenities["Check In Code"]);
    unset($amenities["Check Out Code"]);
    unset($amenities["Query Date Format"]);
    unset($amenities["Adult Guests Code"]);
    unset($amenities["Child Guests Code"]);


    (isset($amenities)) ? update_field('field_5bd0a401aa772', $amenities, $post_id) : '';


}

/**
 * this works through the amenities and returns just the amenities used and their value
 *
 * @param array $amenities array of amenities
 *
 * @return void
 */
function amenities($amenities)
{
    $results = array();
    foreach ($amenities as $amenity) {
        if (strlen($amenity["VALUE"]) > 0 and $amenity["VALUE"] != "0") {
            $results[$amenity["NAME"]] = ($amenity["VALUE"] === 1) ? $amenity["NAME"] : $amenity["VALUE"];
        }
    }
    return $results;
}

function filteredamenities($amenities, $tab)
{
    $results = array();
    foreach ($amenities as $amenity) {
        if ($amenity["AMENITYTABID"] == $tab) {
            if (strlen($amenity["VALUE"]) > 0 and $amenity["VALUE"] != "0") {
                $results[$amenity["NAME"]] = ($amenity["VALUE"] === 1) ? $amenity["NAME"] : $amenity["VALUE"];
            } elseif ( $amenity["NAME"] == "Meeting Type" || $amenity["NAME"] == "Wedding Type" ) {
                $results[$amenity["NAME"]] = $amenity["VALUEARRAY"][0]["LISTVALUE"];
            }
        }
    }
    return $results;
}

function meetingamenities($amenities, $tab)
{
    $results = array();
    foreach ($amenities as $amenity) {
        if ($amenity["AMENITYTABID"] == $tab && $amenity["AMENITYGROUPID"] !== 15 && $amenity["AMENITYGROUPID"] !== 16) {
            if (strlen($amenity["VALUE"]) > 0 and $amenity["VALUE"] != "0") {
                $results[$amenity["NAME"]] = ($amenity["VALUE"] === 1) ? $amenity["NAME"] : $amenity["VALUE"];
            } elseif ( $amenity["NAME"] == "Meeting Type" || $amenity["NAME"] == "Wedding Type" ) {
                $results[$amenity["NAME"]] = $amenity["VALUEARRAY"][0]["LISTVALUE"];
            }
        }
    }
    return $results;
}
