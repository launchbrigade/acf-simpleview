|                   |     |
| ----------------- | --- |
| Country Setting:  | No  |
| Creek/River:      | No  |
| Fountain:         | No  |
| Garden:           | Yes |
| Patio/Courtyard:  | Yes |
| Golf Course View: | No  |
| Natural Setting:  | No  |
| Ocean View:       | No  |
| Panoramic:        | No  |
| Park View:        | No  |
| Redwoods:         | No  |
| Vineyards:        | No  |
| No View:          | No  |
| Yacht/Boat:       | No  |

|                          |     |
| ------------------------ | --- |
| Banquet/Events Facility: | No  |
| Bed and Breakfast:       | No  |
| Church/Chapel:           | No  |
| Conference Center:       | Yes |
| Private Home/Estate:     | No  |
| Hotel:                   | Yes |
| Inn/Lodge:               | No  |
| Park:                    | No  |
| Rehearsal Dinners:       | No  |
| Restaurant:              | Yes |
| Retreat:                 | No  |
| Winery:                  | No  |
